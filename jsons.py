# user
users = [{'_id': 1, 'email': 's1992922@ed.ac.uk', 'nickname': 'Soul', 'games_id': [12, 13, 10, 12], 'reputation':100},
         {'_id': 2, 'email': 'calgusa@gmail.com', 'nickname': 'Bosso', 'games_id': [
             10, 12, 11, 13], 'reputation':0},
         {'_id': 3, 'email': 'probe1@gmail.com', 'nickname': 'Heavylifter',
             'games_id': [10, 12], 'reputation':110},
         {'_id': 4, 'email': 'powerxox@gmail.com', 'nickname': 'Sharpshooter',
             'games_id': [10, 12], 'reputation':230},
         {'_id': 5, 'email': 'thebest@gmail.com',
             'games_id': [10, 12], 'reputation':10},
         {'_id': 6, 'email': 'ilikegames@gmail.com', 'nickname': 'Mongoose',
             'games_id': [10, 12], 'reputation':500},
         {'_id': 7, 'email': 'thisisnotme@gmail.com', 'nickname': 'Bonez',
             'games_id': [10, 12], 'reputation':200}
         ]
# publisher
pubs = [{'_id': 'Ubisoft'},
        {'_id': 'Riot'},
        {'_id': 'Fat shark'},
        {'_id': 'The game co'},
        {'_id': 'Green Box'},
        {'_id': 'Wargaming'},
        {'_id': 'Indie games'},
        {'_id': 'Tensen'},
        {'_id': 'Hammock'},
        ]
# categories
cat = [{'_id': 'action'},
       {'_id': 'rpg'},
       {'_id': 'rogue-like'},
       {'_id': 'multiplayer'},
       {'_id': 'singleplayer'},
       {'_id': 'historic'},
       {'_id': 'casual'},
       ]


# game
games = [{'_id': 12,
          'name': 'Stoneshard',
          'publisher_id': 'Ubisoft',
          'collectibles_id': [7, 8],
          'category_id': [
              'action',
              'rpg'
          ],
          'rating': 4.5},

         {'_id': 10,
          'name': 'Quake',
          'publisher_id': 'Blizzard',
          'collectibles_id': [1, 2],
          'category': ['action',
                       'gore'],
          'rating': 5.5},

         {'_id': 11,
          'name': 'Mastodon',
          'publisher_id': 'Nintendo',
          'collectibles_id': [3, 4],
          'category': ['rpg',
                       'gore'],
          'rating': 9.5},

         {'_id': 13,
          'name': 'As i lay dying',
          'publisher_id': 'Riot',
          'collectibles_id': [5, 6],
          'category': ['rpg',
                       'gore'],
          'rating': 10.5},
         {'_id': 15,
          'name': 'Gore ball',
          'publisher_id': 'Hammock',
          'collectibles_id': [5, 6],
          'category': ['casual'],
          'rating': 8.5},
         ]

colls = [{'_id': 1, 'name': 'Bear', 'MSRP': 2.0},
         {'_id': 2, 'name': 'Paw', 'MSRP': 6.7},
         {'_id': 3, 'name': 'Shiv', 'MSRP': 3.5},
         {'_id': 4, 'name': 'Spear', 'MSRP': 2.2},
         {'_id': 5, 'name': 'Bow', 'MSRP': 5.5},
         {'_id': 6, 'name': 'BFgun', 'MSRP': 2.0},
         {'_id': 7, 'name': 'Biggun', 'MSRP': 5.0},
         {'_id': 8, 'name': 'superNova', 'MSRP': 4.0},
         {'_id': 9, 'name': 'BiGgun', 'MSRP': 3.0}
         ]

# reviews
revs = [{'_id': 2, 'game_id': 10, 'user_id': 'calgusa@gmail.com',
         'rating': 3.4, 'text': 'Best game evar!'},
        {'_id': 3, 'game_id': 11, 'user_id': 's1992922@ed.ac.uk',
         'rating': 5.0, 'text': 'OMG!'},
        {'_id': 4, 'game_id': 10, 'user_id': 'calgusa@gmail.com',
         'rating': 2.9, 'text': 'Best game evar!'},
        {'_id': 5, 'game_id': 10, 'user_id': 's1992922@ed.ac.uk',
         'rating': 3.2, 'text': 'Best game evar!'},
        {'_id': 6, 'game_id': 10, 'user_id': 'calgusa@gmail.com',
         'rating': 4.3, 'text': 'Best game evar!'},
        ]
# -----
load = {
    'publishers': pubs,
    'categories': cat,
    'users': users,
    'games': games,
    'reviews': revs,
    'collectibles': colls
}
