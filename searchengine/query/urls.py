from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('indexing/', views.indexing, name='indexing'),
    path('insert/', views.create, name='insert'),
    path('search/<str:into>/', views.search, name='search'),
    path('filter/<str:into>/', views.filter_of, name='filter')
]
