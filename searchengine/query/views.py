from django.shortcuts import render
import pymongo as pm
import json
from django.http import Http404, HttpResponseBadRequest
# Create your views here.

from django.http import HttpResponse

DB = 'SD'


def index(request):
    return HttpResponse("This is the endpoint to query for info!")


def my_games(req):
    pass


def my_reviews(req):
    pass


def my_collectibles(req):
    pass


def a_game(req):
    pass


def a_collectable(req):
    pass


def indexing(req):
    corpus = json.loads(req.body)
    colls = []
    for k, _ in corpus.items():
        coll = ctx[k]
        coll.create_index([("$**", pm.TEXT)])
        colls.append(k)
    string = " ".join(colls)
    return HttpResponse(f"Indexed:{ string }")


def create(req):
    # return HttpResponse(str(req.body))
    corpus = json.loads(req.body)
    for k, v in corpus.items():
        coll = ctx[k]
        for i in v:
            coll.save(i)
        print(f'db {k}')
        for i in coll.find():
            print(i)
    return HttpResponse("Inserted/Updated")


def read(request):
    pass


def delete(request):
    pass


def update(request):
    pass


def search(req, into=None):
    try:
        tables[into]
    except Exception as e:
        raise Http404('Resource not found.')

    if into:
        try:
            obj = json.loads(req.body)
            query = obj['query']
            try:
                filters = obj['filters']
                query_ = {'$text': {'$search': f'pattern/{query}'}, **filters}
            except:
                query_ = {'$text': {'$search': f'pattern/{query}'}}

            res = [i for i in ctx[into].find(query_)]
            return HttpResponse(json.dumps(res))
        except:
            return HttpResponseBadRequest('malformed query')


def cursor():
    pwd = ''
    with open('pass') as f:
        pwd = f.readline()
    return pm.MongoClient(f"mongodb+srv://super:{pwd}@cluster0-ktykb.mongodb.net/test?retryWrites=true&w=majority")[DB]


tables = {
    'publishers': {},
    'categories': {},
    'users': {},
    'games': {},
    'reviews': {},
    'collectibles': {},
}


def init_filters():
    for table in tables.keys():
        sample = ctx[table].find_one()
        set_ = {key: '' for key in sample.keys() if '_id' not in key}
        tables[table] = set_ if set_ else None
    # print(tables)


def filter_of(req, into=None):
    try:
        filters_ = tables[into]
    except Exception as e:
        raise Http404('Resource not found.')

    return HttpResponse(json.dumps(filters_))


ctx = cursor()
init_filters()
