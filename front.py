import requests

from jsons import load
import importlib
# hotreload
importlib.reload(jsons)
# re-populate
body = requests.get('http://localhost:8000/query/insert/', json=load)._content
# index all collections
body = requests.get(
    'http://localhost:8000/query/indexing/', json=load)._content

# example queries
requests.get('http://localhost:8000/query/search/games/',
             json={'query': 'quake'})._content

requests.get('http://localhost:8000/query/search/collectibles/',
             json={'query': 'Biggun'})._content

# report examples
requests.get('http://localhost:8000/query/filter/games/')._content

requests.get('http://localhost:8000/query/search/games/',
             json={'query': 'gore'})._content

requests.get('http://localhost:8000/query/search/games/',
             json={
                 'query': 'gore', 'filters':
                 {'rating': {'$gt': 8.0}, 'category': 'gore'}
             })._content

requests.get('http://localhost:8000/query/filter/users/')._content


def dump(filename):
    import json
    with open(filename, 'w+') as f:
        json.dump(load, f)


dump('jsons.json')
