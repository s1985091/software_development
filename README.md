### Query Engine and database prototype
> directory hierarchy
> 
    .
    ├── backend
    │   ├── bin
    │   ├── lib
    │   │   └── python3.6
    │   │       └── site-packages
    │   ├── pyvenv.cfg
    │   └── share
    ├── build
    ├── Docs
    ├── front.py
    ├── jsons.json
    ├── jsons.py
    ├── pass
    ├── run
    ├── searchengine
    │   ├── manage.py
    │   ├── query
    │   │   ├── admin.py
    │   │   ├── apps.py
    │   │   ├── __init__.py
    │   │   ├── models.py
    │   │   ├── urls.py
    │   │   └── views.py
    │   └── searchengine
    │       ├── asgi.py
    │       ├── __init__.py
    │       ├── settings.py
    │       ├── urls.py
    │       └── wsgi.py
    └── setup

#### Running the server

assuming you have a working python3 and pip3 installation already running you can:

Install dependencies and build environment
> .$ source setup

Runnning the server
> .$ source run

### Other Application (Section 1.1.3)
Under the directory Application/
