from database import Database
from review_system import ReviewSystem


class Coordinator:
    def __init__(self):
        self.db = Database()

    def daily_updates(self):
        pass

    def weekly_updates(self):
        self._reviews()

    def _reviews(self):
        # TODO : In the future we are going to query only the new updated reviews and guery the games and users that are related with that reviews
        # get the all the users of from the database and store them in a dictionary {id: repuation}
        users = self.db.search("users", {}, {"_id": 1, "reputation": 1})
        users_dictonary = {user['_id']: user['reputation'] for user in users}

        # query the games and store the id in an array
        games = self.db.search("games", {}, {"_id": 1})
        games_id = [game['_id'] for game in games]

        # query all the reviews
        reviews = self.db.search("reviews", {}, {"_id": 1, "game_id": 1, "user_id": 1, "rating": 1, "text": 1})

        # calculate the new ratings for the reviews
        rev_sys = ReviewSystem()
        games_rating = rev_sys.get_games_rating(users_dictonary, games_id, reviews)
        # update each game
        for game_id, rating in games_rating:
            self.db.update("games", {"_id": game_id}, {"rating": rating})


# Mock update of the reviews
coordinator = Coordinator()
coordinator.weekly_updates()
