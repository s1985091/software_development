import pymongo
from settings import Settings


class Database:
    """
    The database contains the main functionalities of interaction with the database
    """

    def __init__(self):
        self.db = pymongo.MongoClient(Settings.db_conn_string)["softwareDevelopment"]
        self.collections = {"categories", "collectables", "games", "publishers", "reviews", "users"}

    def search(self, collection, query, fields):
        """
        a general search query to the database
        :param collection: name of the collection
        :param query: query
        :param fields: the fields that will be returned from the database
        :return: return the results of the query, in a list
        """

        if collection not in self.collections:
            raise Exception("Invalid collection name!")
        return_query = self.db[collection].find(query, fields)
        return [field for field in return_query]

    def update(self, collection, query, updated_values):
        """
        a general update query to the database
        :param collection: the collection of the query
        :param query: the entry that will change valaues
        :param updated_values: the new values of the entry
        """
        if collection not in self.collections:
            raise Exception("Invalid collection name!")

        self.db[collection].update_one(query, {"$set": updated_values})

    def delete(self, collection, query):
        pass

    def insert(self, collection, field):
        pass
