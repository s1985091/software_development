class ReviewSystem:
    """
    This class is responsible for the reviews of the games
    """

    def __init__(self):
        pass

    def _game_rating(self, review_summmary):
        """
        :param review_summmary: array of tuples (review_weight, review_summmary)
        :return: rating of the game
        """
        game_rating = 0
        for review in review_summmary:
            game_rating += review[0] * review[1]
        return game_rating

    def get_games_rating(self, users, games, reviews):

        """
        :param users:  a dictionary of {user_id: reputation}
        :param games: an array of the ids of all the games
        :param reviews: an array of the reviews for all the games
        :return: an array of dictonaries with updated ratings [{_id: game id, new_game_rating}]
        algorithm:
            review_weight = (a * (user_reputation) + b * (votes_of_the_review ) + c) /total weight
            rating_of_game = Σ review_weight * review_rating for all the reviews of the game
        """
        # the constans are subjects to change in the future the values may change
        A = 0.5
        B = 0.3
        C = 0.1
        # the database has no review_votes field and for the initial implementation it will be constant
        review_votes = 1
        games_rating = []
        for game in games:
            total_weight = 0
            reviews_summary = []
            for review in reviews:
                if review['game_id'] == game:
                    # weight of each review
                    review_weight = A * users[review['user_id']] + B * review_votes + C
                    reviews_summary.append((review_weight, review['rating']))
                    total_weight += review_weight

            for i in range(len(reviews_summary)):
                # canonical weight of each review
                reviews_summary[i] = (reviews_summary[i][0] / total_weight, reviews_summary[i][1])

            # TODO remove it in the future, all the games will have some reviews
            if len(reviews_summary) != 0:
                game_rating = self._game_rating(reviews_summary)
                games_rating.append((game, game_rating))
        return games_rating
